from setuptools import setup
import versioneer

requirements = [
    # package requirements go here

]

setup(
    name='managesciproject',
    version=versioneer.get_version(),
    cmdclass=versioneer.get_cmdclass(),
    description="Manage a tree structure of a scientific project with python, JupyteR Notebooks, R and shiny apps on *NIX env.",
    license="GNUv3",
    author="Arnaud Mounier",
    author_email='arnaud.mounier@inrae.fr',
    url='https://forgemia.inra.fr/sciproject/managesciproject',
    packages=['managesciproject'],
    entry_points={
        'console_scripts': [
            'managesciproject=managesciproject.cli:cli'
        ]
    },
    install_requires=requirements,
    keywords='managesciproject',
    classifiers=[
        'Programming Language :: Python :: 3.6',
    ]
)
